${SegmentFile}

${SegmentPrePrimary}
	${If} $Bits = 64
		RegDLL $AppDirectory\GitExtensions\GitExtensionsShellEx32.dll
		RegDLL $AppDirectory\GitExtensions\GitExtensionsShellEx64.dll
	${Else}
		RegDLL $AppDirectory\GitExtensions\GitExtensionsShellEx32.dll
	${EndIf}	
!macroend

${SegmentPostPrimary}
	${If} $Bits = 64
		UnRegDLL $AppDirectory\GitExtensions\GitExtensionsShellEx64.dll
		UnRegDLL $AppDirectory\GitExtensions\GitExtensionsShellEx32.dll
	${Else}
		UnRegDLL $AppDirectory\GitExtensions\GitExtensionsShellEx32.dll
	${EndIf}
!macroend